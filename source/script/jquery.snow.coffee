do ($ = jQuery) ->
  
  #rnd
  rnd = (p...) ->
    r = Math.random()
    #check param
    switch p.length
      when 1
        #check type
        switch $.type p[0]
          #number
          when 'number'
            (r * p[0]) | 0
          #array
          when 'array'
            p[0][(r * p[0].length) | 0]
      when 2
        (p[0] + r * (p[1] - p[0])) | 0
      else (r * 2) | 0
        
  #snow
  $.fn.snow = (param) ->

    #param
    p = $.extend
      html: '&#10052;'
      color: '#fff'
      speed: 1
      size: [8, 24]
      max: 32
      interval: 500
      class: 'flake'
      'z-index': 0
      ani: 'animate'
      rotate: true
    , param

    #set handle
    root = $ window

    wh = root.height()
    ww = root.width()
    ws = root.scrollTop()
    n = 0

    #temp
    temp = $ '<div>'
    .addClass p.class
    .html p.html
    .data timer: {}
    .css
      position: 'absolute'
      top: -96
      cursor: 'default'
      'z-index': p['z-index']

    #listening resize event
    root
    .off 'resize.snow'
    .on 'resize.snow', ->
      clearTimeout temp.data().timer.resize
      temp.data().timer.resize = setTimeout ->
        wh = root.height()
        ww = root.width()
        ws = root.scrollTop()
      , 200
    .off 'scroll.snow'
    .on 'scroll.snow', ->
      clearTimeout temp.data().timer.scroll
      temp.data().timer.scroll = setTimeout ->
        ws = root.scrollTop()
      , 200

    #action
    setInterval ->

      #check num
      if n > p.max
        return

      #size
      size = rnd p.size[0], p.size[1]

      #left
      left = [rnd 96, ww - 96]
      left.push rnd left[0] - 96, left[0] + 96

      #opacity
      opacity = 0.6 + size * 0.02

      #top
      top = [ws - size]
      top.push top[0] + wh
      if top > wh + ws
        top[1] = wh + ws

      #time
      time = rnd wh * size * 0.6, 5e3
      if speed > 0 then time = time / speed

      #action

      #set handle
      flake = temp.clone()

      #prepare
      flake
      .appendTo 'body'
      .css
        left: left[0]
        top: top[0]
        opacity: opacity
        'font-size': size
        color: if ($.type p.color) == 'string' then p.color else rnd p.color

      #animation
      flake[ani]
        left: left[1]
        top: top[1]
        rotate: if p.rotate then rnd 0, 720 else 0
        opacity: 0
      , time, ->
        ($ @).remove()
        n--

      n++
    , p.interval

    #return
    @